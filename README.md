![capturaSymfony.png](https://bitbucket.org/repo/dGMdrq/images/3966059506-capturaSymfony.png)

[Tutorial ](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

que expliqui molt breument els errors del document d'instal·lació que heu seguit, així com els punts que trobeu més difícils.

* El primer error que ens trobem és que hem d'instal·lar php7.0 en comptes del 5, ja que és està obsolet.
**sudo apt-get install git php7.0-cli php7.0-curl acl**
 
* Un dels altres errors, ens diu que hem d'afegir les línes següents al fitxer /etc/mysql/my.cnf, no és correcte, s'ha d'afegir al fitxer: **/etc/mysql/mysql.conf.d/mysqld.cnf** 

collation-server     = utf8mb4_general_ci # Replaces utf8_general_ci
character-set-server = utf8mb4            # Replaces utf8

* També hem d'afegir el fus horari en el següent arxiu **/etc/php/7.0/apache2/php.ini** date.timezone = Europe/Madrid, en canvi, al tutorial ens diu una altra ruta /etc/php5/fpm/php.ini

* Finalment, al arxiu /etc/apache2/sites-available/000-default.conf hi ha un error en les directives. En comptes de Order Allow,Deny hem d'afegir **Require all granted**.